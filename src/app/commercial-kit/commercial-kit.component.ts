import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'commercialkit',
  templateUrl: './commercial-kit.component.html',
  styleUrls: ['./commercial-kit.component.scss']
})
export class CommercialKitComponent implements OnInit {

model = {
	clientID: "",
	campaignID: "",
	startDate: "",
	endDate: "",
	impressions: "",
	budget: "",
	ctrGoal: "",
    viewabilityGoal: ""
	
}

creativeSizes: string[] = [
      '1',
      '2',
      '3',
      '4',
      '5'
]

campaignLines: string[] = [
      'Prospecting',
      'Retargeting',
]

deviceTypes: string[] = [
      'D',
      'M',
      'T',
]

  constructor() { }

  ngOnInit() {
  }

}

