import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';     // Add this
import { AboutComponent } from './about/about.component';
import { LookupMaintenanceComponent } from './lookup-maintenance/lookup-maintenance.component';
import { LookupInventoryComponent } from './lookup-inventory/lookup-inventory.component';
import { AppComponent } from './app.component';
import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { LoginComponent } from './login/login.component';
import { CommercialKitComponent } from './commercial-kit/commercial-kit.component';


const routes: Routes = [

  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'home/lookupMaintenance',
    component: LookupMaintenanceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'home/lookupMaintenance/lookupInventory',
    component: LookupInventoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'home/commercialkit',
    component: CommercialKitComponent,
    canActivate: [AuthGuard]
  }
 ];

 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  bootstrap: [AppComponent]

})
export class AppRoutingModule { }
