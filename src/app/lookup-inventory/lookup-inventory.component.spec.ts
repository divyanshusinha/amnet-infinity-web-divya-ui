import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookupInventoryComponent } from './lookup-inventory.component';

describe('LookupInventoryComponent', () => {
  let component: LookupInventoryComponent;
  let fixture: ComponentFixture<LookupInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookupInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
