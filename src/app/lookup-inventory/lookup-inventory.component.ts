import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Http, Response, Headers } from '@angular/http';
import {Lookups} from '../app-settings/lookups_apis/lookupsAPIEndPoints';

import 'rxjs/add/operator/map'
@Component({
  selector: 'app-lookup-inventory',
  templateUrl: './lookup-inventory.component.html',
  styleUrls: ['../globalStyle.css']
})
export class LookupInventoryComponent implements OnInit {

  lookups = [];
  constructor(private _data: DataService,private _http: Http) { 
    this.getLookupsList();
  }

  ngOnInit() {
  }

  getLookupsList() {
    return this._http.get(Lookups.LOOKUPS_LIST_API_ENDPOINT)
                .map((res: Response) => res.json())
                 .subscribe(data => {
                        this.lookups = data;
                        console.log("from data service"+this.lookups);
                });
  }
}
