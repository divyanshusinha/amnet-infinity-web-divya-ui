export class Environment{
	public static CURRENT_HOST="http://localhost:4200/";
	public static AUTH_URL="https://auth.mediaiqdigital.com/oauth/authorize?grant_type=implicit&response_type=token&client_id=infinity&redirect_uri=";
	public static AUTH_LOGOUT_URL="https://auth.mediaiqdigital.com/logout.do";
	//public static CURRENT_HOST="http://13.54.31.238/";
}