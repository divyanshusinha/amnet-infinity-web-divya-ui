const express = require('express')
const app = express()
var path = require('path')

//app.get('/', (req, res) => res.sendFile(path.join(__dirname + '/index.html'));

	app.get('/quest', function(req, res) {
    res.sendFile(path.join(__dirname + '/app/index.html'));

    console.log("__dirname"+__dirname);
});

app.use('/scripts', express.static(__dirname + '/node_modules/'));
app.use('/controllers', express.static(__dirname + '/app/js/controllers/'));
app.use('/css', express.static(__dirname + '/app/css/'));	
app.use('/images', express.static(__dirname + '/app/img/'));	
app.use('/views', express.static(__dirname + '/app/views/'));	

app.listen(3000, () => console.log('Quest app listening on port 3000!!!'))